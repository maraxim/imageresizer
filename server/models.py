import redis
from rq import Queue, Worker

r = redis.StrictRedis(host='localhost', port=6379, db=0)
if not r.exists('id'):
    r.set('0', '0')

q = Queue(connection=r)

worker = Worker(q)
