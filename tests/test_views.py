from http import HTTPStatus

from flask import url_for


def test_indexs(client):
    response = client.get(url_for('index'))
    assert response.status_code == HTTPStatus.OK
    assert '<h1>Upload new File</h1>' in response.data.decode()
