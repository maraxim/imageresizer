# pylint: skip-file
from flask import Flask

app = Flask(__name__)

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = b'keklol'
# register handlers
from .views import *  # noqa isort:skip
