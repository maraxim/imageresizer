import io
import uuid
from pathlib import Path

from flask import (
    flash,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
)
from PIL import Image
from werkzeug.utils import secure_filename

from .models import q, r
from .wsgi import ALLOWED_EXTENSIONS, app


def allowed_file(filename):
    return (
        '.' in filename
        and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    )


def resize_file(readed_file, size, image_id, extension):
    new_byte_array = io.BytesIO(readed_file)
    original_image = Image.open(new_byte_array)

    new_image = original_image.resize(size)

    img_byte_arr = io.BytesIO()
    new_image.save(img_byte_arr, format=extension)
    new_bytes = img_byte_arr.getvalue()

    new_file_name = image_id + extension

    path = Path('./server')
    path = path / app.config['UPLOAD_FOLDER'] / new_file_name
    path.write_bytes(new_bytes)

    r.set(image_id, new_file_name)
    return 0


@app.route('/uploads/<image_id>', methods=['GET', 'POST'])
def uploads(image_id):
    getted_url = r.get(image_id)
    if getted_url:
        new_file_name = r.get(image_id).decode('utf-8')
        path = Path('./server')
        path = path / app.config['UPLOAD_FOLDER'] / new_file_name

        data = path.read_bytes()
        response = make_response(data)
        response.headers.set('Content-Type', 'image')
        return response

        # 2nd variant:
        # return send_from_directory(path, filename)
    return make_response('not such file found', 404)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        width = request.form['width']
        height = request.form['height']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if not width:
            flash('Widht empty')
            return redirect(request.url)
        if not height:
            flash('Height empty')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            readed_file = file.read()
            extension = filename.rsplit('.')[1]
            if extension.lower() == 'jpg':
                extension = 'jpeg'
            image_id = str(uuid.uuid4())

            new_file_name = image_id + '.' + extension

            path = Path('./server')
            path = path / app.config['UPLOAD_FOLDER'] / new_file_name
            path.write_bytes(readed_file)

            r.set(image_id, new_file_name)

            width = request.form['width']
            height = request.form['height']

            q.enqueue_call(
                func=resize_file,
                args=(
                    readed_file,
                    (int(width), int(height)),
                    image_id,
                    extension,
                ),
            )
            return redirect(url_for('uploads', image_id=image_id))
    return render_template('index.html')
