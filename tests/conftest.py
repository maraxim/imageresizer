import pytest

from server import wsgi


@pytest.fixture(autouse=True)
def app():
    return wsgi.app
