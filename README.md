# Resizer

Сервер, изменяющий размер картинки.
Использованные технологии: Python3, Flask, Jinja2, Redis Queue

## Description

На начальной странице пользователю предлагается загрузить картику и выбрать её желаемый размер.

После загрузки картинки клиент перенаправляется на ресурс, на котором может получить измененную картинку.

До завершения обработки изображения по ссылке доступна старая версия картинки.

Ее новая версия станет доступна по этой же ссылке после завершения обработки изображения воркером.


## API

Начальная страница, предоставляющая пользовательский интерфейс:

http://host:port/

Страница для получения картинки с сервера:

http://host:port/uploads/new_image_name.extension

## Init project

    make init precommit_install

## Run dev server
    
    redis-server
    make up
    
    Для запуска воркера-обработчика queue 
    python3 worker_script.py

	

## Run linters, autoformat and tests

    make pretty lint test
